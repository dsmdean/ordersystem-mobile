'use strict';
angular.module('starter.services', ['ngResource'])
	.constant("baseURL","http://confusion-server.herokuapp.com/")
  	.factory('restaurantFactory', ['$resource', 'baseURL', function($resource, baseURL) {
        return $resource(baseURL+"restaurants/:id",null,{
            'update': {
                method:'PUT' 
            }
        });
  	}])

    .factory('menuFactory', ['$resource', 'baseURL', function($resource, baseURL) {
        return $resource(baseURL+"menuCategory");
    }])

    .factory('faqFactory', ['$resource', 'baseURL', function($resource, baseURL) {
        return $resource(baseURL+"faqQuestions",null,{
            'update': {
                method:'PUT' 
            }
        });
    }])

    .factory('favoriteFactory', ['$resource', '$localStorage', 'baseURL', function($resource, $localStorage, baseURL) {
        var favFac = {};
        var favorites = $localStorage.getObject('favorites', '[]');

        favFac.addToFavorites = function(index) {
            for(var i=0; i<favorites.length; i++) {
                if(favorites[i].id == index)
                    return;
            }

            favorites.push({id: index});
            $localStorage.storeObject('favorites', favorites);
        }

        favFac.getFavorites = function() {
            return favorites;
        }

        favFac.deleteFromFavorites = function(index) {
            for(var i=0; i<favorites.length; i++) {
                if(favorites[i].id == index) {
                    favorites.splice(i, 1);
                }
            }
            $localStorage.storeObject('favorites', favorites);
        }

        return favFac;
    }])

    .factory('cartFactory', ['$resource', '$localStorage', 'baseURL', function($resource, $localStorage, baseURL) {
        var cartFac = {};
        var cart = $localStorage.getObject('cart', '[]');

        cartFac.addToCart = function(index, menuIndex, quantity) {
            for(var i=0; i<cart.length; i++) {
                if(cart[i].menu == menuIndex)
                    return;
            }

            cart.push({id: index, menu: menuIndex, quantity: quantity});
            $localStorage.storeObject('cart', cart);
        }

        cartFac.getCart = function() {
            return cart;
        }

        cartFac.deleteFromCart = function(index) {
            for(var i=0; i<cart.length; i++) {
                if(cart[i].menu == index) {
                    cart.splice(i, 1);
                }
            }
            $localStorage.storeObject('cart', cart);
        }

        return cartFac;
    }])

    .factory('$localStorage', ['$window', function($window) {
        return {
            store: function(ket, value) {
                $window.localStorage[key] = value;
            },
            get: function(key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            storeObject: function(key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function(key, defaultValue) {
                return JSON.parse($window.localStorage[key] || defaultValue);
            }
        }
    }])
;