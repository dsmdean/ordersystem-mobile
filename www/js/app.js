// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'starter.controllers', 'starter.services', 'ksSwiper'])

.run(function($ionicPlatform, $rootScope, $ionicLoading, $timeout) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });

  $rootScope.$on('loading:show', function() {
    $ionicLoading.show({
      template: '<ion-spinner></ion-spinner> Loading ...'
    })
  });

  $rootScope.$on('loading:hide', function() {
    $ionicLoading.hide();
  });

  $rootScope.$on('$stateChangeStart', function() {
    console.log('Loading ...');
    $rootScope.$broadcast('loading:show');
  });

  $rootScope.$on('$stateChangeSuccess', function() {
    console.log('done');
    $rootScope.$broadcast('loading:hide');
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/sidebar.html',
    controller: 'AppCtrl'
  })

  .state('app.howitworks', {
    url: '/howitworks',
    views: {
      'menuContent': {
        templateUrl: 'templates/howitworks.html',
        controller: 'HowItWorksCtrl'
      }
    }
  })

  .state('app.featured', {
    url: '/featured',
    views: {
      'menuContent': {
        templateUrl: 'templates/featured.html',
        controller: 'FeaturedCtrl',
        resolve: {
          restaurants: ['restaurantFactory', function(restaurantFactory) {
            return restaurantFactory.query();
          }]
        }
      }
    }
  })

  .state('app.restaurants', {
    url: '/restaurants',
    views: {
      'menuContent': {
        templateUrl: 'templates/restaurants.html',
        controller: 'RestaurantsCtrl',
        resolve: {
          restaurants: ['restaurantFactory', function(restaurantFactory) {
            return restaurantFactory.query();
          }]
        }
      }
    }
  })

  .state('app.restaurantdetails', {
    url: '/restaurants/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/restaurant-details.html',
        controller: 'RestaurantDetailsCtrl',
        resolve: {
          restaurant: ['restaurantFactory', '$stateParams', function(restaurantFactory, $stateParams) {
            return restaurantFactory.get({id:parseInt($stateParams.id,10)});
          }],
          categories: ['menuFactory', function(menuFactory) {
            return menuFactory.query();
          }]
        }
      }
    }
  })

  .state('app.category', {
    url: '/category/:id/:cat',
    views: {
      'menuContent': {
        templateUrl: 'templates/category-details.html',
        controller: 'CategoryDetailsCtrl',
        resolve: {
          restaurant: ['restaurantFactory', '$stateParams', function(restaurantFactory, $stateParams) {
            return restaurantFactory.get({id:parseInt($stateParams.id,10)});
          }]
        }
      }
    }
  })

  .state('app.product', {
    url: '/product/:id/:dish',
    views: {
      'menuContent': {
        templateUrl: 'templates/product-details.html',
        controller: 'ProductDetailsCtrl',
        resolve: {
          restaurant: ['restaurantFactory', '$stateParams', function(restaurantFactory, $stateParams) {
            return restaurantFactory.get({id:parseInt($stateParams.id,10)});
          }]
        }
      }
    }
  })

  .state('app.faq', {
    url: '/faq',
    views: {
      'menuContent': {
        templateUrl: 'templates/faq.html',
        controller: 'FAQCtrl',
        resolve: {
          questions: ['faqFactory', function(faqFactory) {
            return faqFactory.query();
          }]
        }
      }
    }
  })

  .state('app.favorites', {
    url: '/favorites',
    views: {
      'menuContent': {
        templateUrl: 'templates/favorites.html',
        controller: 'FavoritesCtrl',
        resolve: {
          restaurants: ['restaurantFactory', function(restaurantFactory) {
            return restaurantFactory.query();
          }],
          favorites: ['favoriteFactory', function(favoriteFactory) {
            return favoriteFactory.getFavorites();
          }]
        }
      }
    }
  })

  .state('app.cart', {
    url: '/cart',
    views: {
      'menuContent': {
        templateUrl: 'templates/cart.html',
        controller: 'CartCtrl',
        resolve: {
          restaurants: ['restaurantFactory', function(restaurantFactory) {
            return restaurantFactory.query();
          }],
          cart: ['cartFactory', function(cartFactory) {
            return cartFactory.getCart();
          }]
        }
      }
    }
  })
  ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/howitworks');
});
