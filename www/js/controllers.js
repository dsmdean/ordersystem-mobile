angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };
})

.controller('HowItWorksCtrl', ['$scope', function($scope) {
  $scope.swiper = {};
 
    $scope.onReadySwiper = function (swiper) {
 
        swiper.on('slideChangeStart', function () {
            console.log('slide start');
        });
         
        swiper.on('onSlideChangeEnd', function () {
            console.log('slide end');
        });     
    };
}])

.controller('RestaurantsCtrl', ['$scope', 'restaurantFactory', 'restaurants', 'favoriteFactory', '$ionicListDelegate', '$ionicPlatform', '$cordovaToast', function($scope, restaurantFactory, restaurants, favoriteFactory, $ionicListDelegate, $ionicPlatform, $cordovaToast) {
  $scope.restaurants = restaurants;

  $scope.addFavorite = function(index) {
    //console.log('Added Favorite ' + $scope.restaurants[index].name);

    favoriteFactory.addToFavorites(index);
    $ionicListDelegate.closeOptionButtons();

    $ionicPlatform.ready(function() {
      $cordovaToast
        .show('Added Favorite '+$scope.restaurants[index - 1].name,'long','bottom')
        .then(function(success) {
          // success
          console.log('succes');
        }, function(error) {
          // error
      });
    });
  }
}])

.controller('RestaurantDetailsCtrl', ['$scope', '$stateParams', 'restaurantFactory', 'restaurant', 'categories', 'cartFactory', '$ionicListDelegate', function($scope, $stateParams, restaurantFactory, restaurant, categories, cartFactory, $ionicListDelegate) {
  $scope.restaurant = restaurant;
  $scope.categories = categories;
  $scope.tab = 0;
  $scope.filtText = '';

  /*
   * Tabs select
   */
  $scope.select = function(setTab) {
      $scope.tab = setTab;
      if (setTab === 0) {
        $scope.filtText = "menu";
      } else if (setTab === 1) {
        $scope.filtText = "payments";
      } else if (setTab === 2) {
        $scope.filtText = "pricing";
      } else if (setTab === 3) {
        $scope.filtText = "privacy";
      } else {
        $scope.filtText = "";
      }
    };

    $scope.isSelected = function (checkTab) {
      return ($scope.tab === checkTab);
    };

  $scope.addCart = function(index, name) {
    cartFactory.addToCart(index, name);
    $ionicListDelegate.closeOptionButtons();

    // $ionicPlatform.ready(function() {
    //   $cordovaToast
    //     .show('Added Favorite '+$scope.restaurants[index].name,'long','center')
    //     .then(function(success) {
    //       // success
    //     }, function(error) {
    //       // error
    //   });
    // });
  }
}])

.controller('CategoryDetailsCtrl', ['$scope', '$stateParams', 'restaurantFactory', 'restaurant', 'cartFactory', '$ionicListDelegate', 'filterFilter', '$filter', function($scope, $stateParams, restaurantFactory, restaurant, cartFactory, $ionicListDelegate, filterFilter, $filter) {
  $scope.restaurant = restaurant;
  $scope.cat = parseInt($stateParams.cat,10);
  $scope.menu = $scope.restaurant.menu;

  //$scope.filtered = $filter('categoryFilter')($scope.restaurant.menu,$scope.cat);

  $scope.addCart = function(index, name) {
    cartFactory.addToCart(index, name);
    console.log('Add to cart');
    $ionicListDelegate.closeOptionButtons();

    // $ionicPlatform.ready(function() {
    //   $cordovaToast
    //     .show('Added Favorite '+$scope.restaurants[index].name,'long','center')
    //     .then(function(success) {
    //       // success
    //     }, function(error) {
    //       // error
    //   });
    // });
  }
}])

.controller('ProductDetailsCtrl', ['$scope', '$stateParams', 'restaurant', 'cartFactory', '$ionicPopup', '$ionicPlatform', '$cordovaToast', function($scope, $stateParams, restaurant, cartFactory, $ionicPopup, $ionicPlatform, $cordovaToast) {
  $scope.restaurant = restaurant;
  $scope.dish = parseInt($stateParams.dish,10);
  $scope.quantity = {};

  $scope.porties = [
    { text: "Single", prijs: "$10.00", value: "aa" },
    { text: "Double", prijs: "$12.00", value: "bb" }
  ];

  $scope.data = {
    porties: 'aa'
  };

  $scope.addCart = function(index, name) {
    var AddToCartPopup = $ionicPopup.show({
      title: $scope.restaurant.menu[$scope.dish-1].name,
      subTitle: 'Quantity:',
      templateUrl: 'templates/addtocart-template.html',
      scope: $scope,
      buttons: [
        { text: 'Cancel' },
        {
          text: '<b>Add to cart</b>',
          type: 'button-assertive',
          onTap: function(e) {
            if (!$scope.quantity.number) {
              //don't allow the user to close unless he enters wifi password
              e.preventDefault();
            } else {
              return $scope.quantity.number;
            }
          }
        }
      ]
    });

    AddToCartPopup.then(function(res){
      if(res) {
        console.log('Ok');
        cartFactory.addToCart(index, name, $scope.quantity.number);
        $ionicPlatform.ready(function() {
          $cordovaToast
            .show('Added to cart','long','bottom')
            .then(function(success) {
              // success
            }, function(error) {
              // error
          });
        });
        //$cordovaVibration.vibrate(500);
      } else {
        console.log('Canceled');
      }
    });
  }
}])

.controller('FeaturedCtrl', ['$scope', 'restaurantFactory', 'restaurants', 'favoriteFactory', '$ionicListDelegate', '$ionicPlatform', '$cordovaToast', function($scope, restaurantFactory, restaurants, favoriteFactory, $ionicListDelegate, $ionicPlatform, $cordovaToast) {
  $scope.restaurants = restaurants;

  $scope.addFavorite = function(index) {
    //console.log('Added Favorite ' + $scope.restaurants[index].name);

    favoriteFactory.addToFavorites(index);
    $ionicListDelegate.closeOptionButtons();

    $ionicPlatform.ready(function() {
      $cordovaToast
        .show('Added Favorite '+$scope.restaurants[index - 1].name,'long','bottom')
        .then(function(success) {
          // success
        }, function(error) {
          // error
      });
    });
  }
}])

.controller('FAQCtrl', ['$scope', 'faqFactory', 'questions', function($scope, faqFactory, questions) {
  $scope.tab = 0;
  $scope.filtText = '';
  $scope.questions = questions;
  
  /*
   * FAQ Accordion
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleQuestion = function(question) {
    if ($scope.isQuestionShown(question)) {
      $scope.shownQuestion = null;
    } else {
      $scope.shownQuestion = question;
    }
  };
  $scope.isQuestionShown = function(question) {
    return $scope.shownQuestion === question;
  };

  /*
   * Tabs select
   */
  $scope.select = function(setTab) {
      $scope.tab = setTab;
      if (setTab === 0) {
        $scope.filtText = "";
      } else if (setTab === 1) {
        $scope.filtText = "payments";
      } else if (setTab === 2) {
        $scope.filtText = "pricing";
      } else if (setTab === 3) {
        $scope.filtText = "privacy";
      } else {
        $scope.filtText = "";
      }
    };

    $scope.isSelected = function (checkTab) {
      return ($scope.tab === checkTab);
    };
}])

.controller('CartCtrl', ['$scope', 'restaurants', 'cart', 'baseURL', '$ionicListDelegate', '$ionicPopup', '$ionicLoading', '$timeout', 'cartFactory', '$ionicPlatform', '$cordovaToast', function($scope, restaurants, cart, baseURL, $ionicListDelegate, $ionicPopup, $ionicLoading, $timeout, cartFactory, $ionicPlatform, $cordovaToast) {
  $scope.cart = cart;
  $scope.restaurants = restaurants;

  $scope.grandtotal = function() {
    $scope.total = 0;
    for(var i = 0; i < cart.length; i++) {
      $scope.total += (parseFloat($scope.restaurants[cart[i].id - 1].menu[cart[i].menu - 1].price,10) * cart[i].quantity);
    }
  }

  $scope.quantityPlus = function(id, menu, quantity) {
    var quantityNew = parseInt(quantity,10) + 1;

    cartFactory.deleteFromCart(menu);
    cartFactory.addToCart(id, menu, quantityNew);
  }

  $scope.quantityMinus = function(id, menu, quantity) {
    var quantityNew = parseInt(quantity,10) - 1;

    cartFactory.deleteFromCart(menu);
    cartFactory.addToCart(id, menu, quantityNew);
  }


  $scope.deleteCartItem = function (index) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Confirm Delete',
      template: 'Are you sure you want to delete this item?'
    });

    confirmPopup.then(function(res){
      if(res) {
        console.log('Ok to delete');
        cartFactory.deleteFromCart(index);
        $ionicPlatform.ready(function() {
          $cordovaToast
            .show('Deleted from cart','long','bottom')
            .then(function(success) {
              // success
            }, function(error) {
              // error
          });
        });
        // $cordovaVibration.vibrate(500);
      } else {
        console.log('Canceled delete');
      }
    });
    $scope.shouldShowDelete = false;
  }
}])

.controller('FavoritesCtrl', ['$scope', 'restaurants', 'favorites', 'favoriteFactory', 'baseURL', '$ionicListDelegate', '$ionicPopup', '$ionicLoading', '$timeout', '$ionicPlatform', '$cordovaToast', function($scope, restaurants, favorites, favoriteFactory, baseURL, $ionicListDelegate, $ionicPopup, $ionicLoading, $timeout, $ionicPlatform, $cordovaToast) {
  $scope.baseURL = baseURL;
  $scope.shouldShowDelete = false;

  $scope.favorites = favorites;
  $scope.restaurants = restaurants;

  console.log($scope.restaurants, $scope.favorites);

  $scope.toggleDelete = function () {
    $scope.shouldShowDelete = !$scope.shouldShowDelete;
    console.log($scope.shouldShowDelete);
  }

  $scope.deleteFavorite = function (index) {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Confirm Delete',
      template: 'Are you sure you want to delete this restaurant?'
    });

    confirmPopup.then(function(res){
      if(res) {
        console.log('Ok to delete');
        favoriteFactory.deleteFromFavorites(index);
        $ionicPlatform.ready(function() {
          $cordovaToast
            .show('Deleted from favorites','long','bottom')
            .then(function(success) {
              // success
            }, function(error) {
              // error
          });
        });
        // $cordovaVibration.vibrate(500);
      } else {
        console.log('Canceled delete');
      }
    });
    $scope.shouldShowDelete = false;
  }
}])

.filter('favoriteFilter', function () {
  return function (restaurants, favorites) {
    var out = [];
    for (var i = 0; i < favorites.length; i++) {
      for (var j = 0; j < restaurants.length; j++) {
        if (restaurants[j].id === favorites[i].id)
          out.push(restaurants[j]);
      }
    }
    return out;
  }})

.filter('categoryFilter', function () {
  return function (menu, category) {
    var out = [];
    for (var i = 0; i < menu.length; i++) {
      if(menu[i].category === category) {
        out.push(menu[i]);
      }
    }
    return out;
  }})

.filter('chunkFilter', function () {
  return function (arr, size) {
    var out = [];
    
    for (var i=0; i<arr.length; i+=size) {
      out.push(arr.slice(i, i+size));
    }

    console.log(out);

    return out;
  }});
;